# Milyrama

Exploration en python pour générer des paysages figuratufs inspirés du mycelium, partant à la conquête des espaces humains

![](/images/mily31.png)
![](images/mily2.gif)

## Mily works

In the solution below I used python3.4 as binary, but it's safe to use with any version or binary of python. it works fine on windows too (except the downloading pip with wget obviously but just save the file locally and run it with python).

This is great if you have multiple versions of python installed, so you can manage external libraries per python version.

So first, I'd recommend get-pip.py, it's great to install pip :

wget https://bootstrap.pypa.io/get-pip.py

Then you need to install pip for your version of python, I have python3.4 so for me this is the command :

```
python3.6 get-pip.py
```
Now pip is installed for python3.4 and in order to get libraries for python3.4 one need to call it within this version, like this :
```
python3.6 -m pip
```
So if you want to install numpy you would use :
```
python3.6 -m pip install numpy
```
Note that numpy is quite the heavy library. I thought my system was hanging and failing. But using the verbose option, you can see that the system is fine :
```
python3.6 -m pip install numpy -v
```

### Tests

#### try

```
$ python3 milyrama.py                           

filename mycel_m
SIZE 15000
ZONEWIDTH 80.0
RAD 0.002666666666666667
ZONES 187
one 6.666666666666667e-05
Traceback (most recent call last):
  File "milyrama.py", line 71, in <module>
    class Render(object):
  File "milyrama.py", line 82, in Render
    self.sur = sur
NameError: name 'sur' is not defined

```

### 2to3

```bash
$ 2to3 milyRama.py
-(~/projects)--------------------------------------------------(xavcc@ubiome0)-
`--> 2to3 milyRama.py
RefactoringTool: Skipping optional fixer: buffer
RefactoringTool: Skipping optional fixer: idioms
RefactoringTool: Skipping optional fixer: set_literal
RefactoringTool: Skipping optional fixer: ws_comma
RefactoringTool: Can't parse milyRama.py: ParseError: bad input: type=6, value='', context=('', (378, 0))
RefactoringTool: No files need to be modified.
RefactoringTool: There was 1 error:
RefactoringTool: Can't parse milyRama.py: ParseError: bad input: type=6, value='', context=('', (378, 0))
```

#### pylint

For correction see <http://pylint-messages.wikidot.com/messages:c0103>


```bash
$ `--> pylint --const-rgx='[a-z_][a-z0-9_]{2,30}$' milyRama.py --disable=missing-docstring
************* Module milyRama
milyRama.py:12:0: W0622: Redefining built-in 'int' (redefined-builtin)
milyRama.py:12:0: W0622: Redefining built-in 'any' (redefined-builtin)
milyRama.py:12:0: W0622: Redefining built-in 'all' (redefined-builtin)
milyRama.py:1:0: C0103: Module name "milyRama" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:10:0: C0414: Import alias does not rename original package (useless-import-alias)
milyRama.py:14:0: C0414: Import alias does not rename original package (useless-import-alias)
milyRama.py:15:0: C0414: Import alias does not rename original package (useless-import-alias)
milyRama.py:39:0: C0103: Constant name "filename" doesn't conform to UPPER_CASE naming style (invalid-name)
milyRama.py:71:0: R0205: Class 'Render' inherits from object, can be safely removed from bases in python3 (useless-object-inheritance)
milyRama.py:75:14: E1101: Module 'cairo' has no 'ImageSurface' member (no-member)
milyRama.py:75:33: E1101: Module 'cairo' has no 'FORMAT_ARGB32' member (no-member)
milyRama.py:76:14: E1101: Module 'cairo' has no 'Context' member (no-member)
milyRama.py:82:4: E0602: Undefined variable 'self' (undefined-variable)
milyRama.py:82:15: E0602: Undefined variable 'sur' (undefined-variable)
milyRama.py:83:4: E0602: Undefined variable 'self' (undefined-variable)
milyRama.py:83:15: E0602: Undefined variable 'ctx' (undefined-variable)
milyRama.py:85:4: E0602: Undefined variable 'self' (undefined-variable)
milyRama.py:86:4: E0602: Undefined variable 'self' (undefined-variable)
milyRama.py:71:0: R0903: Too few public methods (0/2) (too-few-public-methods)
milyRama.py:89:0: C0103: Argument name "f" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:91:4: E0401: Unable to import 'Image' (import-error)
milyRama.py:95:4: C0103: Variable name "im" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:96:4: C0103: Variable name "w" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:96:7: C0103: Variable name "h" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:99:13: E0602: Undefined variable 'xrange' (undefined-variable)
milyRama.py:100:17: E0602: Undefined variable 'xrange' (undefined-variable)
milyRama.py:101:12: C0103: Variable name "r" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:101:15: C0103: Variable name "g" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:101:18: C0103: Variable name "b" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:110:0: C0103: Argument name "x1" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:110:0: C0103: Argument name "y1" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:110:0: C0103: Argument name "x2" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:110:0: C0103: Argument name "y2" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:118:0: C0103: Argument name "x" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:118:0: C0103: Argument name "y" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:118:0: C0103: Argument name "r" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:124:0: C0103: Argument name "x" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:124:0: C0103: Argument name "y" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:124:0: C0103: Argument name "r" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:130:0: C0103: Argument name "x1" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:130:0: C0103: Argument name "y1" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:130:0: C0103: Argument name "x2" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:130:0: C0103: Argument name "y2" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:130:0: C0103: Argument name "r" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:130:0: R0913: Too many arguments (6/5) (too-many-arguments)
milyRama.py:130:0: R0914: Too many local variables (16/15) (too-many-locals)
milyRama.py:132:4: C0103: Variable name "dx" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:133:4: C0103: Variable name "dy" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:134:4: C0103: Variable name "dd" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:136:4: C0103: Variable name "n" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:137:4: C0103: Variable name "n" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:139:4: C0103: Variable name "a" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:144:4: C0103: Variable name "xp" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:145:4: C0103: Variable name "yp" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:148:8: C0103: Variable name "x" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:148:11: C0103: Variable name "y" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:153:0: C0103: Argument name "x1" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:153:0: C0103: Argument name "y1" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:153:0: C0103: Argument name "x2" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:153:0: C0103: Argument name "y2" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:153:0: C0103: Argument name "r" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:153:0: R0913: Too many arguments (6/5) (too-many-arguments)
milyRama.py:155:4: C0103: Variable name "dx" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:156:4: C0103: Variable name "dy" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:157:4: C0103: Variable name "a" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:160:4: C0103: Variable name "xp" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:160:47: E0602: Undefined variable 'LINE_NOISE' (undefined-variable)
milyRama.py:161:4: C0103: Variable name "yp" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:161:47: E0602: Undefined variable 'LINE_NOISE' (undefined-variable)
milyRama.py:165:8: C0103: Variable name "x" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:165:11: C0103: Variable name "y" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:170:0: C0103: Argument name "x1" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:170:0: C0103: Argument name "y1" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:170:0: C0103: Argument name "x2" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:170:0: C0103: Argument name "y2" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:170:0: R0913: Too many arguments (6/5) (too-many-arguments)
milyRama.py:170:0: R0914: Too many local variables (18/15) (too-many-locals)
milyRama.py:172:4: C0103: Variable name "dx" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:173:4: C0103: Variable name "dy" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:174:4: C0103: Variable name "dd" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:175:4: C0103: Variable name "a" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:177:4: C0103: Variable name "xp" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:178:4: C0103: Variable name "yp" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:180:4: C0103: Variable name "r" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:180:7: C0103: Variable name "g" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:180:10: C0103: Variable name "b" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:183:8: C0103: Variable name "x" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:183:11: C0103: Variable name "y" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:188:0: C0103: Argument name "x" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:188:0: C0103: Argument name "y" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:188:0: C0103: Argument name "Z" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:192:4: C0103: Variable name "ij" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:195:4: C0103: Variable name "it" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:197:4: W0612: Unused variable 'inds' (unused-variable)
milyRama.py:199:0: E0104: Return outside function (return-outside-function)
milyRama.py:202:0: W0101: Unreachable code (unreachable)
milyRama.py:199:7: E0602: Undefined variable 'inds' (undefined-variable)
milyRama.py:202:0: C0103: Argument name "x" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:202:0: C0103: Argument name "y" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:206:4: C0103: Variable name "z" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:210:0: R0914: Too many local variables (28/15) (too-many-locals)
milyRama.py:215:4: C0103: Variable name "Z" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:215:21: E0602: Undefined variable 'xrange' (undefined-variable)
milyRama.py:217:4: C0103: Variable name "R" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:218:4: C0103: Variable name "X" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:219:4: C0103: Variable name "Y" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:220:4: C0103: Variable name "THE" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:221:4: C0103: Variable name "GE" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:222:4: C0103: Variable name "P" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:223:4: C0103: Variable name "C" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:224:4: C0103: Variable name "D" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:231:8: C0103: Variable name "x" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:232:8: C0103: Variable name "y" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:240:8: E0103: 'continue' not properly in loop (not-in-loop)
milyRama.py:259:4: C0103: Variable name "z" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:266:4: C0103: Variable name "ti" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:288:12: C0103: Variable name "r" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:296:12: W0101: Unreachable code (unreachable)
milyRama.py:296:12: C0103: Variable name "ge" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:301:12: C0103: Variable name "x" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:302:12: C0103: Variable name "y" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:321:16: E1111: Assigning result of a function call, where the function has no return (assignment-from-no-return)
milyRama.py:329:16: W0101: Unreachable code (unreachable)
milyRama.py:330:15: C1801: Do not use `len(SEQUENCE)` to determine if a sequence is empty (len-as-condition)
milyRama.py:331:16: C0103: Variable name "dd" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:349:16: C0103: Variable name "z" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:353:16: E1101: Instance of 'Render' has no 'ctx' member (no-member)
milyRama.py:354:16: E1101: Instance of 'Render' has no 'circles' member (no-member)
milyRama.py:365:16: E1101: Instance of 'Render' has no 'sur' member (no-member)
milyRama.py:367:16: C0103: Variable name "ti" doesn't conform to snake_case naming style (invalid-name)
milyRama.py:267:4: W0612: Unused variable 'drawn' (unused-variable)
milyRama.py:210:0: R0912: Too many branches (15/12) (too-many-branches)
milyRama.py:210:0: R0915: Too many statements (87/50) (too-many-statements)
milyRama.py:378:0: E0104: Return outside function (return-outside-function)
milyRama.py:380:0: W0101: Unreachable code (unreachable)
milyRama.py:381:4: W0125: Using a conditional statement with a constant value (using-constant-test)
milyRama.py:385:8: C0103: Constant name "pfilename" doesn't conform to UPPER_CASE naming style (invalid-name)
milyRama.py:387:8: C0103: Constant name "p" doesn't conform to UPPER_CASE naming style (invalid-name)
milyRama.py:12:0: W0611: Unused any imported from numpy (unused-import)
milyRama.py:12:0: W0611: Unused all imported from numpy (unused-import)

--------------------------------------------------------------------
Your code has been rated at -0.36/10 (previous run: -2.54/10, +2.19)

```
